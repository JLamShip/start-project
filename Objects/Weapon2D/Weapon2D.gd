class_name Weapon2D
extends Node2D

@export var bullet_scene: PackedScene
@export var fire_range := 200.0

var fire_range_property:
	get: 
		return fire_range
	set(value):
		fire_range = value
		
		if not is_inside_tree():
			await self
		_range_shape.radius = fire_range

@export var fire_cooldown := 1.0
@export var _bullet_spawn_position := $BulletSpawnPosition2D
@export var _cooldown_timer := $CooldownTimer
@export var _range_area := $RangeArea2D
@export var _animation_player := $AnimationPlayer
@export var _range_shape:CircleShape2D = $RangeArea2D/CollisionShape2d.shape

func _ready():
	fire_range_property = fire_range

func _physics_process(delta):
	if not _cooldown_timer.is_stopped():
		return

	var targets: Array = _range_area.get_overlapping_areas()
	if targets.is_empty():
		return	
	var target: Node2D = targets[0]
	shoot_at(target.global_position)

func shoot_at(target_position: Vector2) -> void:
	look_at(target_position)
	_animation_player.play("shoot")

	var bullet = bullet_scene.instance()
	add_child(bullet)
	bullet.global_position = _bullet_spawn_position.global_position

	bullet.fly_to(target_position)
	_cooldown_timer.start(fire_cooldown)
