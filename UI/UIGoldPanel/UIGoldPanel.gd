extends Control


@onready var _label: Label = $Label

var player: Player :
	get:
		return player # TODOConverter40 Non existent get function 
	set(mod_value):
		mod_value  # TODOConverter40 Copy here content of set_player

func set_player(new_player: Player) -> void:
	player = new_player
	player.connect("gold_changed",Callable(self,"_update_gold_amount"))
	_update_gold_amount(player.gold)


func _update_gold_amount(new_amount: int) -> void:
	_label.text = "Gold: %s" % new_amount
