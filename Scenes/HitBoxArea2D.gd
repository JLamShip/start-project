extends Area2D
class_name HitBoxArea2D

@export_enum("PLAYER", "ENEMY") var team : int
@export var damage := 1
@export var can_hit_multiple := false

func apply_hit(hurt_box: HurtBoxArea2D) -> void:
	hurt_box.get_hurt(damage)
	set_deferred("monitoring", can_hit_multiple)

func _on_area_entered(area: Area2D) -> void:
	apply_hit(area)
	
