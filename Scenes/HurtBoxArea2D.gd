class_name HurtBoxArea2D
extends Area2D

signal hit_landed(damage)

@export_enum("PLAYER", "ENEMY") var team : int
@export var armor := 0

func get_hurt(damage: int) -> void:
	var final_damage := damage - armor
	emit_signal("hit_landed", final_damage)
